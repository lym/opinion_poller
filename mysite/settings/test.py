# Load defaults in order to then add/override with test-only settings

from defaults import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME'  : 'mysite_test'
    }
}
