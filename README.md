This is a miniature polling application. A poll is basically a question with
various options for answers. A user selects one of or a couple of
options as their response.

An example of a poll would be:

    Who would you like to be NRM's representative in the forthcoming
    Uganda elections?
    - Amama Mbabazi
    - Museveni Yoweri
    - None

The application consists of two parts:
- The public (user-facing) side that lets users view polls and cast
  their votes.
- An admin side where polls are created, altered and seleted.

# Requirements
- python version 2.7 or higher
- django version 1.7
- postgresql 9.2 or higher

# Getting started
- clone this repository

- create a database with the name <i>mysite_development</i> for user
  <i>django_mysite</i>:

      $ sudo su - postgres

      $ createdb mysite_development

      $ createuser -P -e mysite_development

      $ psql -d mysite_development

      # GRANT ALL ON DATABASE mysite_development TO django_mysite;

      # \q

      $ logout

Create the necessary tables in the database:

      $ python manage.py migrate

- From the project root, start the server with the command:
      $ python manage.py runserver

- Point your browser to [user-facing side](http://127.0.0.1/polls/)

- Point your browser to [admin side](http://127.0.0.1/admin)

# Running Tests

    $ python manage.py test --settings=mysite.settings.test polls

# TODO
- Add command for seeding data before starting the database so we have
  some sample data to work with.

- Add link to django tutorials page which inspired this application.
